package com.raone.azimut.ui.main


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator

import com.raone.azimut.R
import com.raone.azimut.modules.QrHash
import kotlinx.android.synthetic.main.fragment_main.*

/**
 *
 */
class MainFragment : Fragment() {
    private var listener: IMain? = null
    private val auth = FirebaseAuth.getInstance()
    // Access a Cloud Firestore instance from your Activity
    val db = FirebaseFirestore.getInstance()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IMain) {
            listener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvSignOut.setOnClickListener {
            Log.i("main", "MF:  sighOut()")
            listener?.signOut()
        }

        tvWelcome.text = "${auth.currentUser!!.email}"

        icScanQr.setOnClickListener {
            IntentIntegrator(activity).initiateScan()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Log.i("main", "onActivityResult: Cancelled.")
            } else {
                if (tvWelcome != null) {
                    var text = result.contents

                    if (text.contains("r1:")) {
                        val data = text.replace("r1:", "").split( "|")[0]
                        val refId = text.split("|")[1]

                        val qrHash = Gson().fromJson<QrHash>(data, QrHash::class.java)

                        Log.i("mainMain", "refId=$refId, qrHash: $qrHash")

                        val hashes = db.collection("hashes")
                        if (qrHash != null) {
                            if (qrHash.hash.isNotEmpty()) {

                                hashes.document(refId)
                                    .get()
                                    .addOnSuccessListener { document ->
                                        if (document != null) {
                                            Log.d("mainMain", "DocumentSnapshot data: ${document.data}")
                                            val map = document.data!!
                                            map["email"] = "${auth.currentUser!!.email}"


                                            Log.d("mainMain", "DocumentSnapshot map: $map")
                                            hashes.document(refId).set(map)
                                        } else {
                                            Log.d("mainMain", "No such document")
                                        }
                                    }
                                    .addOnFailureListener { exception ->
                                        Log.d("mainMain", "get failed with ", exception)
                                    }

                            }
                        }
                    }

                    tvWelcome.text = result.contents
                }
                Log.i("main", "onActivityResult: Scanned, text=\" -> { '${result.contents}  \": ${result.orientation}\"' } <- \"")
            }
        }
    }

    interface IMain {
        fun signOut()
    }
}
