package com.raone.azimut.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.raone.azimut.R
import com.raone.azimut.ui.auth.fragment.LoginFragment
import com.raone.azimut.ui.main.MainFragment

class MainActivity : AppCompatActivity(), LoginFragment.ILogin, MainFragment.IMain {
    private var currentFramgnet = -1

    override fun finishWork() {
        finish()
    }

    private lateinit var auth: FirebaseAuth

    override fun signOut() {
        auth.signOut()
        checkUser()
    }

    override fun logedIn() {
        checkUser()
    }

    /*
    * Sign Up.
    * */
    override fun signUp() {
        currentFramgnet = FRAGMENT_SIGN_UP
        replaceFragmentWithStack(LoginFragment.instanceSighUp(), "LoginFragmentSignUp()")
    }

    /*
    * Log in.
    * */
    override fun login() {
        currentFramgnet = FRAGMENT_LOGIN
        replaceFragmentWithStack(LoginFragment.instanceLogin(), "LoginFragmentLogin()")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkUser()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        when (currentFramgnet) {
            FRAGMENT_LOGIN -> {
                finish()
            }

            FRAGMENT_MAIN -> {
                finish()
            }
        }
    }

    private fun checkUser() {
        auth = FirebaseAuth.getInstance()

        if (auth.currentUser == null) {
            login()
        } else {
            showMain()
        }
    }

    private fun showMain() {
        currentFramgnet = FRAGMENT_MAIN
        replaceFragmentWithStack(MainFragment(), "MainFragment")
    }

    private fun addFragmentWithStack(fragment: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.flMainContainer, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }

    private fun replaceFragmentWithStack(fragment: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.flMainContainer, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        supportFragmentManager.fragments.forEach { it.onActivityResult(requestCode, resultCode, data) }
    }

    companion object {
        const val FRAGMENT_LOGIN = 1
        const val FRAGMENT_SIGN_UP = 2
        const val FRAGMENT_MAIN = 3
    }
}
