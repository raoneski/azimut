package com.raone.azimut.ui.auth.fragment

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raone.azimut.*

import com.raone.azimut.base.BaseFragment
import com.raone.azimut.const.colorGray
import com.raone.azimut.const.colorRed
import com.raone.azimut.manager.SettingsManager
import com.raone.azimut.modules.presenter.LoginPresenter
import com.raone.azimut.modules.presenter.SettingsPresenter
import com.raone.azimut.view.ILoginView
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.loading.*

class LoginFragment(val type: Int = FRAGMENT_SIGHUP) : BaseFragment(), ILoginView {
    override fun loading() {
        if (rlLoading == null) return

        rlLoading.visibility = View.VISIBLE
        pbLoading.visibility = View.VISIBLE
        icCheckLogin.visibility = View.GONE
        btnLogin.isEnabled = false
        tvLoadingMessage.setText(R.string.loading)

        rlLoadingBlackout.setOnClickListener {
            hideLoading()
        }
    }

    override fun hideLoading() {
        if (rlLoading == null) return

        btnLogin.isEnabled = true
        rlLoading.visibility = View.GONE
        pbLoading.visibility = View.GONE
        icCheckLogin.visibility = View.GONE
    }

    override fun success() {
        pbLoading.visibility = View.GONE

        icCheckLogin.setImageResource(R.drawable.ic_check)
        icCheckLogin.visibility = View.VISIBLE
        btnLogin.isEnabled = true

        listener?.logedIn()
    }

    override fun error(messge: String) {
        pbLoading.visibility = View.GONE

        icCheckLogin.setImageResource(R.drawable.ic_cancel_circle_red)
        icCheckLogin.visibility = View.VISIBLE
        tvLoadingMessage.text = getString(R.string.error_message).replace("{message}", messge)
    }

    private var listener: ILogin? = null
    private var settingsPresenter = SettingsPresenter()
    private lateinit var loginPresenter: LoginPresenter


    init {
        Azimut.instance.component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        loginPresenter = LoginPresenter()
        loginPresenter.onAttach(this)

        view.etEmail.text = settingsPresenter.loadString(SettingsManager.PROFILE_USER_EMAIL).toEditable()

        if (type == FRAGMENT_SIGHUP) {
            view.tvTitle.setText(R.string.sign_up)
            view.btnLogin.setText(R.string.sign_up)
            view.tvNewOrHave.setText(R.string.signup_login)
            view.tvSighUp.setText(R.string.login)
        } else if (type == FRAGMENT_LOGIN) {
            view.tvTitle.setText(R.string.login)
            view.btnLogin.setText(R.string.login)
            view.tvNewOrHave.setText(R.string.login_signup)
            view.tvSighUp.setText(R.string.login_signup_btn)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        icBack.setOnClickListener {
            when(type) {
                FRAGMENT_LOGIN -> {
                    listener?.finishWork()
                }

                FRAGMENT_SIGHUP -> {
                    listener?.login()
                }
            }
        }

        tvSighUp.setOnClickListener {
            when(type) {
                FRAGMENT_LOGIN -> {
                    listener?.signUp()
                }

                FRAGMENT_SIGHUP -> {
                    listener?.login()
                }
            }
        }

        etEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.i("main", "LF: etEmail - $s")
                if (s!!.isNotEmpty()) {
                    settingsPresenter.saveString(SettingsManager.PROFILE_USER_EMAIL, s.toString())
                }
            }

        })

        btnLogin.setOnClickListener {
            etEmail.hideKeyboard()
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()

            var a = false
            var b = false

            Log.i("main", "Login: $email ::: $password")
            if ((email.isNotNullOrEmpty())) {
                etEmail.setHintTextColor(colorGray)
                a = true
            } else {
                etEmail.setHintTextColor(colorRed)
                a = false
            }

            if ((password.isNotNullOrEmpty())) {
                etPassword.setHintTextColor(colorGray)
                b = true
            } else {
                etPassword.setHintTextColor(colorRed)
                b = false
            }


            if (a && b) {
                login(email, password)
            }
        }
    }

    private fun login(email: String, password: String) {
        when(type) {
            FRAGMENT_SIGHUP -> {
                loginPresenter.signUp(email, password)
            }

            FRAGMENT_LOGIN -> {
                loginPresenter.login(email, password)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ILogin) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
        loginPresenter.onDetach()
        settingsPresenter.onDetach()
    }

    interface ILogin {
        fun logedIn()
        fun signUp()
        fun login()
        fun finishWork()
    }


    companion object {
        fun instanceSighUp() = LoginFragment(FRAGMENT_SIGHUP)
        fun instanceLogin() = LoginFragment(FRAGMENT_LOGIN)
    }
}
