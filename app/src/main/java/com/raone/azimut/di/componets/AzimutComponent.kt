package com.raone.azimut.di.componets

import com.raone.azimut.Azimut
import com.raone.azimut.base.BaseFragment
import com.raone.azimut.di.module.AzimutModule
import com.raone.azimut.di.module.InteractorModule
import com.raone.azimut.di.module.ManagerModule
import com.raone.azimut.interactor.SettingsInteractor
import com.raone.azimut.modules.presenter.SettingsPresenter
import com.raone.azimut.ui.MainActivity
import com.raone.azimut.ui.auth.fragment.LoginFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AzimutModule::class,
    ManagerModule::class,
    InteractorModule::class])
interface AzimutComponent {
    fun inject(application: Azimut)

    // Interactor.
    fun inject(settingsInteractor: SettingsInteractor)

    // Presenter.
    fun inject(settingsPresenter: SettingsPresenter)

    // Activity.
    fun inject(mainActivity: MainActivity)
    fun inject(baseFragment: BaseFragment)
    fun inject(loginFragment: LoginFragment)
}