package com.raone.azimut.di.module

import android.content.SharedPreferences
import com.raone.azimut.interactor.SettingsInteractor
import com.raone.azimut.manager.SettingsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class InteractorModule {

    @Provides
    @Singleton
    fun provideSettingsInteractor(manager: SettingsManager) = SettingsInteractor(manager)
}