package com.raone.azimut.di.module

import android.app.Application
import android.content.Context
import com.raone.azimut.Azimut
import com.raone.azimut.util.GREEN_LAB_PREFERENCES
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AzimutModule (
    private val azimut: Azimut
) {
    @Provides
    @Singleton
    fun provideApplication(): Application {
        return azimut
    }

    @Provides
    fun provideSharedPreferences() = azimut.getSharedPreferences(GREEN_LAB_PREFERENCES, Context.MODE_PRIVATE)

}