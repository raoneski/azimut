package com.raone.azimut.di.module

import android.content.SharedPreferences
import com.raone.azimut.manager.SettingsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ManagerModule {

    @Provides
    @Singleton
    fun provideSettingsManager(preferences: SharedPreferences) = SettingsManager(preferences)
}