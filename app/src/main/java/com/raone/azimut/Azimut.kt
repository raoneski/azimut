package com.raone.azimut

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.raone.azimut.di.componets.AzimutComponent
import com.raone.azimut.di.componets.DaggerAzimutComponent
import com.raone.azimut.di.module.AzimutModule

class Azimut: Application() {
    lateinit var component: AzimutComponent

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        instance = this
        setup()
    }

    private fun setup() {
        component = DaggerAzimutComponent.builder()
            .azimutModule(AzimutModule(this)).build()
        component.inject(this)
    }


    fun azimutComponent(): AzimutComponent {
        return component
    }

    companion object {
        lateinit var instance: Azimut private set
    }
}