package com.raone.azimut.view

interface ILoginView {
    fun loading()
    fun hideLoading()
    fun success()
    fun error(messge: String)
}