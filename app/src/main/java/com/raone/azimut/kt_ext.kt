package com.raone.azimut

import android.content.Context
import android.inputmethodservice.InputMethodService
import android.text.Editable
import android.view.View
import android.view.inputmethod.InputMethodManager

fun String?.isNotNullOrEmpty(): Boolean {
    return this != null && this.isNotEmpty()
}

fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)


fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}