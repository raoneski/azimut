package com.raone.azimut.modules.presenter

import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.raone.azimut.base.BasePresenter
import com.raone.azimut.view.ILoginView

class LoginPresenter: BasePresenter<ILoginView>() {
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()

    fun signUp(email: String, password: String) {
        Log.i("main", "LP: signUp($email, $password)")
        mainView?.loading()

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = auth.currentUser
                    Log.i("main", "createUserWithEmail:success: ${user.toString()}")
                    mainView?.success()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.i("main", "createUserWithEmail:failure", task.exception)
                    mainView?.error(task.exception.toString())
                }
            }

    }

    fun login(email: String, password: String) {
        Log.i("main", "LP: login($email, $password)")

        mainView?.loading()

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    Log.i("main", "signInWithEmailAndPassword:success: ${user.toString()}")
                    mainView?.success()
                } else {

                    Log.i("main", "signInWithEmailAndPassword:success: ${task.exception.toString()}")
                    mainView?.error(task.exception.toString())
                }
            }
    }
}