package com.raone.azimut.modules

data class QrHash(
    val hash: String,
    val ip: String,
    var email: String,
    var synched: Boolean
)